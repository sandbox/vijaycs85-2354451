<?php

/**
 * @file
 * Builds placeholder replacement tokens for help topics.
 *
 * This file handles tokens for the global 'help_topic' tokens.
 */

use Drupal\help\Entity\HelpTopic;

/**
 * Implements hook_token_info().
 */
function help_token_info() {
  $types['help_topic'] = array(
    'name' => t("Help Topics"),
    'description' => t("Provides tokens for help topics."),
  );

  $topics = array();
  /* @var \Drupal\help\HelpTopicInterface $help_topic */
  foreach (HelpTopic::loadMultiple() as $help_topic_id => $help_topic) {
    $topics[$help_topic_id] = array(
      'name' => $help_topic->label(),
      'description' => t('URL to the @label help topic', array('@label' => $help_topic->label())),
    );
  }

  return array(
    'types' => $types,
    'tokens' => array(
      'help_topic' => $topics,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function help_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'help_topic') {
    foreach ($tokens as $name => $original) {
      if ($topic = HelpTopic::load($name)) {
        $replacements[$original] = $topic->url('canonical');
      }
    }
  }

  return $replacements;
}
