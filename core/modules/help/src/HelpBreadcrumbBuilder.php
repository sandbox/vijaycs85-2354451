<?php

/**
 * @file
 * Contains \Drupal\help\HelpBreadcrumbBuilder.
 */

namespace Drupal\help;

use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Provides a breadcrumb builder for help topic pages.
 */
class HelpBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * Constructs the BookBreadcrumbBuilder.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation service.
   */
  public function __construct(TranslationInterface $string_translation) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $route_match->getRouteName() == 'entity.help_topic.canonical';
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $links = array(
      Link::createFromRoute($this->t('Home'), '<front>'),
      Link::createFromRoute($this->t('Administration'), 'system.admin'),
      Link::createFromRoute($this->t('Help'), 'help.main'),
    );

    return $links;
  }

}
