<?php

/**
 * @file
 * Contains \Drupal\help\Controller\HelpController.
 */

namespace Drupal\help\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\Component\Utility\String;
use Drupal\help\Entity\HelpTopic;
use Drupal\help\HelpTopicInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller routines for help routes.
 */
class HelpController extends ControllerBase {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The help topic entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $helpStorage;

  /**
   * The token replacement service class.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Creates a new HelpController.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Entity\EntityStorageInterface $help_storage
   *   The entity storage for help topic entities.
   * @param \Drupal\Core\Utility\Token $token
   *   The token replacement service class.
   */
  public function __construct(RouteMatchInterface $route_match, EntityStorageInterface $help_storage, Token $token) {
    $this->routeMatch = $route_match;
    $this->helpStorage = $help_storage;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('entity.manager')->getStorage('help_topic'),
      $container->get('token')
    );
  }

  /**
   * Prints a page listing help topics.
   *
   * @return array
   *   Render array for the help topics list.
   */
  public function helpMain() {
    $output = array(
      '#attached' => array(
        'css' => array(drupal_get_path('module', 'help') . '/css/help.module.css'),
      ),
    );

    $template = '<h2>{{ title }}</h2><p>{{ header }}</p>{{ links }}';

    $output['modules'] = array(
      '#type' => 'inline_template',
      '#template' => $template,
      '#context' => array(
        'title' => $this->t('Module help'),
        'header' => $this->t('Help pages are available for the following modules:'),
        'links' => array('#markup' => $this->moduleHelpLinksAsList()),
      ),
    );

    $output['topics'] = array(
      '#type' => 'inline_template',
      '#template' => $template,
      '#context' => array(
        'title' => $this->t('Configured topics'),
        'header' => $this->t('Additional help topics configured on your site:'),
        'links' => array('#markup' => $this->helpTopicsList()),
      ),
    );

    return $output;
  }

  /**
   * Provides a formatted list of available module help topics from hook_help().
   *
   * @return string
   *   A string containing the formatted list.
   */
  protected function moduleHelpLinksAsList() {
    $module_info = system_rebuild_module_data();

    $modules = array();
    foreach ($this->moduleHandler()->getImplementations('help') as $module) {
      if ($this->moduleHandler()->invoke($module, 'help', array("help.page.$module", $this->routeMatch))) {
        $modules[$module] = array(
          'title' => $module_info[$module]->info['name'],
          'url' => new Url('help.page', array('name' => $module)),
        );
      }
    }
    asort($modules);

    return $this->fourColumnList($modules);
  }

  /**
   * Makes a four-column list of links.
   *
   * @param array $links
   *   Array whose elements are arrays with:
   *   - title: Link title.
   *   - url: URL object.
   *
   * @return string
   *   Markup for a four-column UL list of links.
   */
  protected function fourColumnList($links) {
    // Output pretty four-column list.
    $count = count($links);
    $break = ceil($count / 4);
    $output = '<div class="clearfix"><div class="help-items"><ul>';
    $i = 0;
    foreach ($links as $info) {
      $output .= '<li>' . $this->l($info['title'], $info['url']) . '</li>';
      if (($i + 1) % $break == 0 && ($i + 1) != $count) {
        $output .= '</ul></div><div class="help-items' . ($i + 1 == $break * 3 ? ' help-items-last' : '') . '"><ul>';
      }
      $i++;
    }
    $output .= '</ul></div></div>';

    return $output;
  }

  /**
   * Provides a formatted list of configured help topics.
   *
   * @return string
   *   A string containing the formatted list.
   */
  protected function helpTopicsList() {
    $entities = $this->helpStorage->loadMultiple();
    uasort($entities, array('Drupal\help\Entity\HelpTopic', 'sort'));

    $topics = array();
    /* @var \Drupal\help\HelpTopicInterface $entity */
    foreach ($entities as $entity) {
      if ($entity->isTopLevel()) {
        $topics[] = array(
          'title' => $entity->label(),
          'url' => $entity->urlInfo('canonical'),
        );
      }
    }

    return $this->fourColumnList($topics);
  }

  /**
   * Renders a help page from a module's hook_help().
   *
   * @param string $name
   *   A module name to display a help page for.
   *
   * @return array
   *   A render array for the help page.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function helpPage($name) {
    $build = array();
    if ($this->moduleHandler()->implementsHook($name, 'help')) {
      $info = system_get_info('module');
      $build['#title'] = String::checkPlain($info[$name]['name']);

      $temp = $this->moduleHandler()->invoke($name, 'help', array("help.page.$name", $this->routeMatch));
      if (empty($temp)) {
        $build['top']['#markup'] = $this->t('No help is available for module %module.', array('%module' => $info[$name]['name']));
      }
      else {
        $build['top']['#markup'] = $temp;
      }

      // Only print list of administration pages if the module in question has
      // any such pages associated to it.
      $admin_tasks = system_get_module_admin_tasks($name, $info[$name]);
      if (!empty($admin_tasks)) {
        $links = array();
        foreach ($admin_tasks as $task) {
          $link['url'] = $task['url'];
          $link['title'] = $task['title'];
          $links[] = $link;
        }
        $build['links']['#links'] = array(
          '#heading' => array(
            'level' => 'h3',
            'text' => $this->t('@module administration pages', array('@module' => $info[$name]['name'])),
          ),
          '#links' => $links,
        );
      }
      return $build;
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * Renders a help topic page from a configured help entity.
   *
   * @param \Drupal\help\HelpTopicInterface $help_topic
   *   The help entity to display.
   *
   * @return array
   *   A render array for the help topic page.
   */
  public function helpEntityView(HelpTopicInterface $help_topic) {
    $build = array();

    $build['#title'] = String::checkPlain($help_topic->label());

    $body = $help_topic->getBody();
    $build['body'] = array(
      '#type' => 'processed_text',
      '#text' => $this->token->replace($body['value']),
      '#format' => $body['format'],
    );

    $related = $help_topic->getRelated();
    $links = array();
    foreach ($related as $other_id) {
      if ($other_id != $help_topic->id()) {
        $topic = $this->helpStorage->load($other_id);
        if ($topic) {
          $links[] = array(
            'title' => $topic->label(),
            'url' => $topic->urlInfo('canonical'),
          );
        }
      }
    }

    if (count($links)) {
      $build['related'] = array(
        '#theme' => 'links',
        '#heading' => array(
          'text' => $this->t('Related topics'),
          'level' => 'h3',
        ),
        '#links' => $links,
      );
    }

    // @todo Also make a list of topics that list this one as "related"?

    return $build;
  }

}
