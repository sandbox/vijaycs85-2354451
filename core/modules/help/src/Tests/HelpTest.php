<?php

/**
 * @file
 * Definition of Drupal\help\Tests\HelpTest.
 */

namespace Drupal\help\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Verify help display and user access to help based on permissions.
 *
 * @group help
 */
class HelpTest extends WebTestBase {

  // Install with the standard profile, because it has the help block
  // enabled and admin theme, etc.
  protected $profile = 'standard';

  /**
   * Modules to enable.
   *
   * @var array.
   */
  public static $modules = array('help_test');

  /**
   * The admin user that will be created.
   */
  protected $adminUser;

  /**
   * The anonymous user that will be created.
   */
  protected $anyUser;

  protected function setUp() {
    parent::setUp();

    // Create users.
    $this->adminUser = $this->drupalCreateUser(array('access administration pages', 'view the administration theme', 'administer permissions', 'administer help topics'));
    $this->anyUser = $this->drupalCreateUser(array());
  }

  /**
   * Logs in users, creates dblog events, and tests dblog functionality.
   */
  public function testHelp() {
    // Login the admin user.
    $this->drupalLogin($this->adminUser);
    $this->verifyHelp();
    $this->verifyHelpLinks();

    // Login the regular user.
    $this->drupalLogin($this->anyUser);
    $this->verifyHelp(403);

    // Check for css on admin/help.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/help');
    $this->assertRaw('help.module.css', 'The help.module.css file is present in the HTML.');

    // Verify that introductory help text exists, goes for 100% module coverage.
    $this->assertRaw(t('For more information, refer to the subjects listed in the Help Topics section or to the <a href="!docs">online documentation</a> and <a href="!support">support</a> pages at <a href="!drupal">drupal.org</a>.', array('!docs' => 'https://drupal.org/documentation', '!support' => 'https://drupal.org/support', '!drupal' => 'https://drupal.org')), 'Help intro text correctly appears.');

    // Verify that help topics text appears.
    $this->assertRaw('<h2>' . t('Module help') . '</h2><p>' . t('Help pages are available for the following modules:') . '</p>', 'Help module topics text correctly appears.');
    $this->assertRaw('<h2>' . t('Configured topics') . '</h2><p>' . t('Additional help topics configured on your site:') . '</p>', 'Help configured topics text correctly appears.');

    // Make sure links are properly added for modules implementing hook_help().
    foreach ($this->getModuleList() as $module => $name) {
      $this->assertLink($name, 0, format_string('Link properly added to @name (admin/help/@module)', array('@module' => $module, '@name' => $name)));
    }

    // Make sure links are properly added for topics.
    foreach ($this->getTopicList() as $topic => $name) {
      $this->assertLink($name, 0, format_string('Link properly added to @name (admin/help-topic/@topic)', array('@topic' => $topic, '@name' => $name)));
    }
  }

  /**
   * Verifies the logged in user has access to the various help nodes.
   *
   * @param integer $response
   *   An HTTP response code.
   */
  protected function verifyHelp($response = 200) {
    $this->drupalGet('admin/index');
    $this->assertResponse($response);
    if ($response == 200) {
      $this->assertText('This page shows you all available administration tasks for each module.');
    }
    else {
      $this->assertNoText('This page shows you all available administration tasks for each module.');
    }

    foreach ($this->getModuleList() as $module => $name) {
      // View module help node.
      $this->drupalGet('admin/help/' . $module);
      $this->assertResponse($response);
      if ($response == 200) {
        $this->assertTitle($name . ' | Drupal', format_string('%module title was displayed', array('%module' => $module)));
        $this->assertRaw('<h1 class="page-title">' . t($name) . '</h1>', format_string('%module heading was displayed', array('%module' => $module)));
      }
    }

    foreach ($this->getTopicList() as $topic => $name) {
      // View module help node.
      $this->drupalGet('admin/help-topic/' . $topic);
      $this->assertResponse($response);
      if ($response == 200) {
        $this->assertTitle($name . ' | Drupal', format_string('%topic title was displayed', array('%topic' => $topic)));
        $this->assertRaw('<h1 class="page-title">' . t($name) . '</h1>', format_string('%topic heading was displayed', array('%topic' => $topic)));
      }
    }
  }

  /**
   * Verifies links on the test help topic page and other pages.
   *
   * Assumes an admin user is logged in.
   */
  protected function verifyHelpLinks() {
    // Verify links on the test top-level page.
    $page = 'admin/help-topic/help_test';
    $links = array(
      'link to the Help module topic' => 'The Help module provides',
      'link to the help admin page' => 'Add new help topic',
      'Help module' => 'The Help module provides',
      'Linked topic' => 'This topic is not supposed to be top-level',
    );
    foreach ($links as $link_text => $page_text) {
      $this->drupalGet($page);
      $this->clickLink($link_text);
      $this->assertText($page_text);
    }

    // Verify that the non-top-level topic does not appear on the Help page.
    $this->drupalGet('admin/help');
    $this->assertNoLink('Linked topic');
  }

  /**
   * Gets a list of modules to test for hook_help() pages.
   *
   * @return array
   *   A list of modules to test, machine name => displayed name.
   */
  protected function getModuleList() {
    return array(
      'help_test' => 'Help Test',
    );
  }

  /**
   * Gets a list of topic IDs to test.
   *
   * @return array
   *   A list of topics to test, machine name => displayed name.
   */
  protected function getTopicList() {
    return array(
      'help' => t('Help module'),
      'help_test' => t('Help Test module'),
    );
  }

}
