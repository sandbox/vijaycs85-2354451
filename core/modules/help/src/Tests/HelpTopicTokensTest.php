<?php

/**
 * @file
 * Contains \Drupal\help\Tests\HelpTopicTokensTest.
 */

namespace Drupal\help\Tests;

use Drupal\Core\Url;
use Drupal\simpletest\KernelTestBase;

/**
 * Generates URLs replacements to test token generation for help topics.
 *
 * @group system
 */
class HelpTopicTokensTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('system', 'help');

  /**
   * Sets up the test.
   */
  protected function setUp() {
    parent::setUp();
    $this->installSchema('system', array('router', 'sequences'));
    $this->installConfig(array('help'));
    \Drupal::service('router.builder')->rebuild();

  }

  /**
   * Tests that help topic tokens work.
   */
  public function testHelpTopicTokens() {
    $text = 'This should <a href="[help_topic:help]">Link to help topic</a>';
    $replaced = \Drupal::token()->replace($text);
    $this->assertTrue(strpos($replaced, '<a href="' . Url::fromRoute('entity.help_topic.canonical', [
      'help_topic' => 'help',
    ])->toString() . '"') !== FALSE);

    $text = 'This should <a href="[help_topic:nonexistant]">Not link to help topic</a>';
    $replaced = \Drupal::token()->replace($text);
    $this->assertTrue(strpos($replaced, '[help_topic:nonexistant]') !== FALSE);
  }

}
