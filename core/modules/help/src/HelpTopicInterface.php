<?php

/**
 * @file
 * Contains \Drupal\help\HelpTopicInterface.
 */

namespace Drupal\help;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines an interface for a help topic entity.
 */
interface HelpTopicInterface extends ConfigEntityInterface {

  /**
   * Returns the body of the topic.
   *
   * @return array
   *   Array with elements:
   *   - value: Unformatted, unfiltered text.
   *   - format: ID of the text format to use to filter/format the text.
   */
  public function getBody();

  /**
   * Returns whether this is a top-level topic or not.
   *
   * @return bool
   *   TRUE if this is a topic that should be displayed on the Help topics
   *   list; FALSE if not.
   */
  public function isTopLevel();

  /**
   * Returns the IDs of related topics.
   *
   * @return array
   *   Array of the IDs of related topics.
   */
  public function getRelated();

  /**
   * Sets the related topics
   *
   * @param string|array
   *   Either an array of related topic IDS, or a comma-separated list of
   *   related topic IDs.
   *
   * @return $this
   */
  public function setRelated($topics);

}
