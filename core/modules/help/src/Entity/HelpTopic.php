<?php

/**
 * @file
 * Contains \Drupal\help\Entity\HelpTopic.
 */

namespace Drupal\help\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\help\HelpTopicInterface;

/**
 * Defines a configuration entity for help topics.
 *
 * Developers can provide configurable help topics for their modules, themes,
 * etc. by creating help topics from admin/config/development/help (with the
 * Help module installed), and then exporting them into their config/install
 * directories. Topics marked as "top_level" will be listed on admin/help,
 * and when viewing a help topic, "related" topics will be listed. Conventions:
 * - Module overview help topics should have titles like "Foo Bar module";
 *   similar for overview topics for themes, install profiles, etc.
 * - Module overview topics should follow
 *   @link https://drupal.org/node/632280 the standard help template. @endlink
 * - Non-overview help topics should usually not be top-level topics; instead,
 *   make an overview topic and list other topics in the "related" section.
 * - All topic machine names should be prefixed by the extension machine name.
 *   The main topic for an extension can just be the bare machine name.
 * - Keep in mind that help topics are usually meant for administrative Drupal
 *   users, such as site builders and content editors, rather than programmers.
 *
 * @ConfigEntityType(
 *   id = "help_topic",
 *   label = @Translation("Help topic"),
 *   config_prefix = "topic",
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\help\Form\HelpTopicForm",
 *       "edit" = "Drupal\help\Form\HelpTopicForm",
 *       "delete" = "Drupal\help\Form\HelpDeleteForm",
 *     },
 *     "list_builder" = "Drupal\help\HelpListBuilder",
 *     "access" = "Drupal\help\HelpAccessControlHandler",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   admin_permission = "administer help topics",
 *   links = {
 *     "canonical" = "entity.help_topic.canonical",
 *     "add-form" = "entity.help_topic.add_form",
 *     "edit-form" = "entity.help_topic.edit_form",
 *     "delete-form" = "entity.help_topic.delete_form"
 *   }
 * )
 */
class HelpTopic extends ConfigEntityBase implements HelpTopicInterface {
  /**
   * The topic machine name.
   *
   * @var string
   */
  protected $id;

  /**
   * The topic title.
   *
   * @var string
   */
  protected $label;

  /**
   * The unfiltered topic body text.
   *
   * This is an array with two elements:
   * - value: The unfiltered text.
   * - format: The ID of the filter format.
   *
   * @var array
   */
  protected $body;

  /**
   * Whether or not the topic should appear on the help topics list.
   *
   * @var bool
   */
  protected $top_level;

  /**
   * List of related topic machine names.
   *
   * @var string[]
   */
  protected $related = array();

  /**
   * {@inheritdoc}
   */
  public function getBody() {
    return $this->get('body');
  }

  /**
   * {@inheritdoc}
   */
  public function isTopLevel() {
    return $this->get('top_level');
  }

  /**
   * {@inheritdoc}
   */
  public function getRelated() {
    return $this->get('related');
  }

  /**
   * {@inheritdoc}
   */
  public function setRelated($topics) {
    // $topics can be an array or a comma-separated string.
    if (is_string($topics)) {
      $topics = explode(',', $topics);
    }

    // Make a list of non-empty, trimmed IDs.
    $tosave = array();
    foreach ($topics as $item) {
      $item = trim($item);
      if ($item) {
        $tosave[] = $item;
      }
    }

    $this->set('related', $tosave);

    return $this;
  }
}

