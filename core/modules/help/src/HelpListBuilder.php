<?php

/**
 * @file
 * Contains \Drupal\help\HelpListBuilder.
 */

namespace Drupal\help;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an entity list builder class for Help topic entities.
 */
class HelpListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = array();

    $header['label'] = $this->t('Title');
    $header['id'] = $this->t('Machine name');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = array();

    $row['label']['data'] = array(
      '#type' => 'link',
      '#title' => $this->getLabel($entity),
      '#url' => $entity->urlInfo('canonical'),
    );

    $row['id'] = $entity->id();

    return $row + parent::buildRow($entity);
  }
}
