<?php

/**
 * @file
 * Contains \Drupal\help\HelpAccessControlHandler.
 */

namespace Drupal\help;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines entity access for Help topic entities.
 */
class HelpAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, $langcode, AccountInterface $account) {
    // The default for the parent class is fine for CRUD operations. But
    // for viewing, use the same permission as for viewing help topics provided
    // by hook_help().
    if ($operation == 'view') {
      return AccessResult::allowedIfHasPermission($account, 'access administration pages');
    }

    return parent::checkAccess($entity, $operation, $langcode, $account);
  }
}
