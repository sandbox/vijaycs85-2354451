<?php

/**
 * @file
 * Contains \Drupal\help\Form\HelpTopicForm.
 */

namespace Drupal\help\Form;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for editing a help topic.
 */
class HelpTopicForm extends EntityForm {

  /**
   * The help topic entity storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $helpStorage;

  /**
   * Constructs a new help topic form.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $help_storage
   *   The help topic entity storage
   */
  public function __construct(ConfigEntityStorageInterface $help_storage) {
    $this->helpStorage = $help_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('help_topic')
    );
  }

  /**
   * Checks for an existing help topic.
   *
   * @param string $entity_id
   *   The entity ID.
   *
   * @return bool
   *   TRUE if this topic already exists, FALSE otherwise.
   */
  public function exists($entity_id) {
    return (bool) $this->helpStorage
      ->getQuery()
      ->condition('id', $entity_id)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 100,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => array(
        'exists' => array($this, 'exists'),
        'error' => $this->t('The machine-readable name must be unique, and can only contain lowercase letters, numbers, and underscores.'),
      ),
    );

    $form['top_level'] = array(
      '#type' => 'checkbox',
      '#default_value' => $this->entity->isTopLevel(),
      '#title' => $this->t('Top-level topic'),
      '#description' => $this->t('Check box if this topic should be displayed on the topics list'),
    );

    $body = $this->entity->getBody();

    $form['body'] = array(
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => $body['value'],
      '#format' => $body['format'],
    );

    $form['related'] = array(
      '#title' => $this->t('Related topics'),
      '#description' => $this->t('Comma-separated list of machine names of related topics.'),
      '#type' => 'textarea',
      '#default_value' => implode(',', $this->entity->getRelated()),
    );

    $form['#entity_builders'][] = array($this, 'copyRelatedFieldToEntity');

    return parent::form($form, $form_state);
  }

  /**
   * Copies the related topics field value to the entity property.
   *
   * This is added to $form['#entity_builders'] in the form builder method.
   *
   * @param string $type
   *   Type of entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to copy property values to.
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  protected function copyRelatedFieldToEntity($type, EntityInterface $entity, array &$form, FormStateInterface &$form_state) {
    $entity->setRelated($form_state->getValue('related'));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('help.topic_admin');

    $status = $this->entity->save();
    if ($status == SAVED_UPDATED) {
      drupal_set_message($this->t('Help topic updated.'));
    }
    else {
      drupal_set_message($this->t('Help topic added.'));
    }
  }
}
