<?php

/**
 * @file
 * Contains \Drupal\system\Tests\Token\RouteTokensTest.
 */

namespace Drupal\system\Tests\Token;

use Drupal\Core\Url;
use Drupal\simpletest\KernelTestBase;

/**
 * Generates URLs replacements to test token generation for routes.
 *
 * @group system
 */
class RouteTokensTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('system', 'user');

  /**
   * Sets up the test.
   */
  protected function setUp() {
    parent::setUp();
    $this->installSchema('system', array('router', 'sequences'));
    $this->installConfig(array('core'));
    \Drupal::service('router.builder')->rebuild();

  }

  /**
   * Tests that default route tokens work.
   */
  public function testRouteTokens() {
    $text = 'This should <a href="[route:system.admin]">Link to admin</a>';
    $replaced = \Drupal::token()->replace($text);
    $this->assertTrue(strpos($replaced, '<a href="' . Url::fromRoute('system.admin')->toString() . '"') !== FALSE);

    $text = 'This should <a href="[route:system.nonexistant]">Not link to admin</a>';
    $replaced = \Drupal::token()->replace($text);
    $this->assertTrue(strpos($replaced, '[route:system.nonexistant]') !== FALSE);
  }

}
